FROM registry.access.redhat.com/ubi8/s2i-base

LABEL MAINTAINER="Tobias Florek <tf@schaeffer-ag.de>"

RUN curl -qfL https://github.com/openshift/source-to-image/releases/download/v1.1.14/source-to-image-v1.1.14-874754de-linux-amd64.tar.gz | tar xzC /usr/local/bin/

COPY ./rootfs/ /
